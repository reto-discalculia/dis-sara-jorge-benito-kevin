/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package discalculia;

import java.util.ArrayList;

/**
 *
 * @author dam113
 */
public class Numero { 
    private int num;
    private String simbolo;
    private String num_escrito;

    public Numero(int num, String num_escrito) {
        this.num = num;
        this.num_escrito = num_escrito;
    } 

    public Numero(String simbolo, String num_escrito) {
        this.simbolo = simbolo;
        this.num_escrito = num_escrito;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
    
    
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNum_escrito() {
        return num_escrito;
    }

    public void setNum_escrito(String num_escrito) {
        this.num_escrito = num_escrito;
    }
    
    
}
